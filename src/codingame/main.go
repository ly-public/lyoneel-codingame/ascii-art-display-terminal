package main

import "fmt"
import "os"
import "bufio"
import "strings"

func main() {

	const scanBuffer = 1000000
	const letterQty int = 26
	const asciiOffset = 65
	const asciiZ = 90

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Buffer(make([]byte, scanBuffer), scanBuffer)

	var L int
	scanner.Scan()
	fmt.Sscan(scanner.Text(), &L)

	var H int
	scanner.Scan()
	fmt.Sscan(scanner.Text(), &H)

	scanner.Scan()
	T := strings.ToUpper(scanner.Text())

	abcArr := [letterQty]string{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "W", "X", "Y", "Z", "?"}
	arrLength := L * letterQty
	artArr := make([]string, H)
	var outputString string

	fmt.Fprintln(os.Stderr, "L:         ", L)
	fmt.Fprintln(os.Stderr, "H:         ", H)
	fmt.Fprintln(os.Stderr, "arrLength: ", arrLength)
	fmt.Fprintln(os.Stderr, "abcArr:    ", abcArr)
	fmt.Fprintln(os.Stderr, "START OF artArr:     See lines below")

	for i := 0; i < H; i++ {
		scanner.Scan()
		artArr[i] = scanner.Text()
		fmt.Fprintln(os.Stderr, artArr[i])
	}

	fmt.Fprintln(os.Stderr, "END OF artArr.")
	fmt.Fprintln(os.Stderr, "T (text to process): ", T)

	for i := 0; i < H; i++ { // Height
		outputString = ""
		for j := 0; j < len(T); j++ { // Letter

			letterAscii := int(T[j])
			var letterIdx int

			if letterAscii >= asciiOffset && letterAscii <= asciiZ {
				letterIdx = letterAscii - asciiOffset
			} else {
				letterIdx = letterQty
			}

			letterIdx *= L
			wordSlice := artArr[i][letterIdx : letterIdx+L]
			/*
			   fmt.Fprintln(os.Stderr, "letterAscii: ", letterAscii)
			   fmt.Fprintln(os.Stderr, "letterIndex: ", letterIdx)
			   fmt.Fprintln(os.Stderr, "wordSlice:   ", wordSlice)
			*/
			outputString += wordSlice
		}
		fmt.Println(outputString)
	}

}
